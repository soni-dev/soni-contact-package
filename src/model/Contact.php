<?php

namespace sonipkg\contact\model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
     protected $fillable = [
        'name', 'email', 'message',
    ];
}