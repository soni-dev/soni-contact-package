<?php

namespace sonipkg\contact\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use sonipkg\contact\Mail\ContactMailable;
use Mail;
use sonipkg\contact\model\Contact;
class ContactFormController extends Controller
{
  public function contactForm(){
  	return view("contact::contactform");
  }
public function saveContactForm(Request $request){
   Contact::create($request->all());
	Mail::to(config("contact.send_email_to"))->send(new ContactMailable($request->message,$request->name));
	//Mail::to($request->email)->send(new ContactMailable($request->message,$request->name));
  	return redirect()->back()->with("success","mail send successfully");
  }

}
